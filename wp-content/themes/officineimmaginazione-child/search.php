<main class="search-main container" id="search">
	<?php if (have_posts()) : ?>
		<h2 class="d-flex align-items-center flex-column search-title">
			<span><?php _e("Risultati per la ricerca", "officineimmaginazione"); ?></span>
			<span><?php echo $s ?></span>
		</h2>
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class("row wine"); ?> id="wine-<?php the_ID(); ?>">
				<div class="col-12 col-md-6">
					<a class="d-block" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
						<div class="wine-img-container">
							<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
						</div>
					</a>
				</div>
				<div class="d-flex flex-wrap col-12 col-md-6 wine-details-container">
					<div class="align-self-start w-100 wine-title-box">
						<h5 class="wine-date"><?php echo get_the_date("d F Y"); ?></h5>
						<h3 class="wine-title">
							<a class="d-block" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
								<span class="d-block"><?php the_title(); ?></span>
								<?php $productor = get_post_meta(get_the_ID(), 'wd_productor', true); ?>
								<?php if($productor) : ?>
									<span class="d-block"><?php echo $productor; ?></span>
								<?php endif; ?>
							</a>
						</h3>
					</div>
					<div class="align-self-center w-100 wine-excerpt">
						<span class="d-block"><?php echo wp_trim_words(get_the_content(), 20); ?></span>
						<a class="btn-dots" href="<?php the_permalink(); ?>" ><span></span></a>
					</div>
					<div class="align-self-end w-100 share-box">
						<span><?php _e("Condividi", "officineimmaginazione"); ?></span>
						<span id="share-<?php echo get_the_ID() ?>"></span>
					</div>
					<script async type="text/javascript">
						jQuery(document).ready(function($) {
							$("#share-<?php echo get_the_ID() ?>").jsSocials({
								url: "<?php the_permalink(); ?>",
								showLabel: false,
								showCount: false,
								shares: [
									{share: "facebook", logo: "fab fa-facebook"},
									{share: "pinterest", logo: "fab fa-pinterest"}
								]
							});
						});
					</script>
				</div>
			</article>
		<?php endwhile; ?>
		<div class="text-center">
			<?php oi_pagination(); ?>
		</div>
	<?php else : ?>
		<h2 class="d-flex align-items-center flex-column search-title">
			<span><?php _e("Nessun risultato per la ricerca", "officineimmaginazione"); ?></span>
			<span><?php echo $s ?></span>
		</h2>
		<h3 class="text-center"><?php _e("Torna a", "officineimmaginazione"); ?></h3>
		<div class="text-center">
			<a class="btn btn-default mt-3" href="<?php echo get_category_link(1); ?>" title=""><?php echo get_cat_name(1); ?></a>
			<a class="btn btn-default mt-3" href="<?php echo get_category_link(3); ?>" title=""><?php echo get_cat_name(3); ?></a>
		</div>
	<?php endif; ?>
</main>