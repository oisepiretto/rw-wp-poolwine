<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	
	$prefix = "io_";
	
	// Initiate your meta box
	$options = new OI_Meta_Box(
		array(
		"id" => "institutional_options_mb",
		"title" => "Opzioni pagina",
		"pages" => array("page"),
		"templates" => array("page-institutional.php"),
		"context" => "side",
		"priority" => "high",
	));

	$options->addImage($prefix . "img-0", array("name" => "Immagine", "desc" => "Immagine animazione su immagine in evidenza", "gid" => 1));
	$options->addImage($prefix . "img-1", array("name" => "Immagine", "desc" => "Immagine animazione su immagine in evidenza", "gid" => 2));
	
	$options->addText($prefix . "subtitle", array("name" => "Sottotitolo", "desc" => "Scegli come vuoi intitolare il paragrafo", "gid" => 3));

	// Finish Meta Box Declaration 
	$options->Finish();
	
	
	$prefix = "id_";
	
	// Initiate your meta box
	$description = new OI_Meta_Box(
		array(
		"id" => "institutional_description_mb",
		"title" => "Descrizione",
		"pages" => array("page"),
		"templates" => array("page-institutional.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$description->addImage($prefix . "img", array("name" => "Immagine", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$description->addText($prefix . "title", array("name" => "Titolo", "desc" => "Inserisci un titolo per il paragrafo", "gid" => 2));
	$description->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto", "gid" => 2));

	// Finish Meta Box Declaration 
	$description->Finish();
	
	$prefix = "idt_";
	
	// Initiate your meta box
	$details = new OI_Meta_Box(
		array(
		"id" => "institutional_details_mb",
		"title" => "Dettagli",
		"pages" => array("page"),
		"templates" => array("page-institutional.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$details->addImage($prefix . "img", array("name" => "Immagine", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$details->addText($prefix . "title", array("name" => "Titolo", "desc" => "Inserisci un titolo per il paragrafo", "gid" => 2));
	$details->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto", "gid" => 2));
    $details->addImage($prefix . "mobile-banner", array("name" => "Banner mobile", "desc" => "Scegli quale immagine comparirà come banner su mobile", "gid" => 3));

	// Finish Meta Box Declaration 
	$details->Finish();
	
}
