<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	
	$prefix = "cd_";
	
	// Initiate your meta box
	$details = new OI_Meta_Box(
		array(
		"id" => "contact_details_mb",
		"title" => "Dettagli contatti",
		"pages" => array("page"),
		"templates" => array("page-contacts.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$details->addImage($prefix . "img", array("name" => "Immagine", "desc" => "Seleziona un'immagine di sfondo per la pagina", "gid" => 1));
	
	$details->addText($prefix . "title", array("name" => "Titolo", "desc" => "Scegli come vuoi intitolare il paragrafo", "gid" => 2));
	$details->addText($prefix . "shortcode", array("name" => "Shortcode", "desc" => "Inserisci uno shortcode, verrà generato", "gid" => 2));

	$details->addTextList($prefix . "mailto", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Link 1", "gid" => 3));
	$details->addTextList($prefix . "tel", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Link 2", "gid" => 3));

	// Finish Meta Box Declaration 
	$details->Finish();
	
}
