<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	
	$prefix = "hi_";
	
	// Initiate your meta box
	$intro = new OI_Meta_Box(
		array(
		"id" => "homepage_intro_mb",
		"title" => "Intro",
		"pages" => array("page"),
		"templates" => array("homepage.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$intro->addText($prefix . "subtitle", array("name" => "Sottotitolo", "desc" => "Inserisci un sottotitolo per il paragrafo", "gid" => 1));
	$intro->addImage($prefix . "img", array("name" => "Immagine", "desc" => "Scegli quale immagine comparirà sotto il testo", "gid" => 2));
	$intro->addTextList($prefix . "btn", array("txt" => "Testo del pulsante", "url" => "Url link"), array("name" => "Pulsante", "gid" => 2));

	// Finish Meta Box Declaration 
	$intro->Finish();
	
	$prefix = "id_";
	
	// Initiate your meta box
	$details = new OI_Meta_Box(
		array(
		"id" => "homepage_details_mb",
		"title" => "Dettagli",
		"pages" => array("page"),
		"templates" => array("homepage.php"),
		"context" => "normal",
		"priority" => "high",
	));

	$details->addImage($prefix . "img-0", array("name" => "Immagine 1", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$details->addImage($prefix . "img-1", array("name" => "Immagine 2", "desc" => "Scegli quale immagine comparirà a fianco del testo", "gid" => 1));
	$details->addText($prefix . "title", array("name" => "Titolo", "desc" => "Inserisci un titolo per il paragrafo", "gid" => 2));
	$details->addTextList($prefix . "btn", array("txt" => "Testo del link", "url" => "Url link"), array("name" => "Pulsante", "gid" => 2));
	$details->addWysiwyg($prefix . "content", array("name" => "Contenuto", "desc" => "Inserisci del contenuto", "gid" => 3));
    $details->addImage($prefix . "mobile-banner", array("name" => "Banner Mobile", "desc" => "Scegli quale immagine comparirà nel banner in formato mobile", "gid" => 4));

	// Finish Meta Box Declaration 
	$details->Finish();
	
	
}
