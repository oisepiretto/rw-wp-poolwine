<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
	$post_id = $_GET["post"] ? $_GET["post"] : $_POST["post_ID"] ;
    if(has_category(icl_object_id(3, 'category', false), $post_id)) {
		// Initiate your meta box
		$details = new OI_Meta_Box(
			array(
			"id" => "wine_details_mb",
			"title" => "Dettagli vino",
			"pages" => array("post"),
			//"templates" => array(""),
			"context" => "normal",
			"priority" => "high",
		));
		
		$details->addText("wd_productor", array("name" => "Produttore", "desc" => "Chi è il produttore del vino?", "gid" => 1));
		$details->addText("wd_type", array("name" => "Tipo vino", "desc" => "Che tipo di vino è?", "gid" => 1));
		$details->addText("wd_location", array("name" => "Location", "desc" => "Scegli la location", "gid" => 1));
		
		$details->addText("wd_artist", array("name" => "Artista", "desc" => "Inserisci il nome dell'artista", "gid" => 10));
		$details->addText("wd_song_title", array("name" => "Titolo della canzone", "desc" => "Inserisci il titolo della canzone", "gid" => 10));
		$details->addText("wd_song_url", array("name" => "Link alla canzone", "desc" => "Inserisci l'url per sentire la canzone", "gid" => 10));
		

		// Finish Meta Box Declaration 
		$details->Finish();
	}
}
