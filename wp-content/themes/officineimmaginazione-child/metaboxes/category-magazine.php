<?php

//include the main class file
include(get_template_directory(). "/metaboxes/class/meta-box-class.php");

if (is_admin()) {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if(has_category(icl_object_id(1, 'category', false), $post_id)) {
		// Initiate your meta box
		$side = new OI_Meta_Box (
			array(
			'id' => 'magazine_mb',
			'title' => 'Opzioni aggiuntive',
			'pages' => array('post'),
			'context' => 'side',
			'priority' => 'high',
		));

		// 
		$side->addFile('magazine_pdf', array('name' => 'Seleziona un file', 'gid' => 1));

		// Finish Meta Box Declaration 
		$side->Finish();
	}
}
