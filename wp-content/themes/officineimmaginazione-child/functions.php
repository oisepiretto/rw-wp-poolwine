<?php
/*
Theme Name: Officine Immaginazione Child
Theme URI: http://www.officineimmaginazione.com/wordpress-themes
Description: OfficineImmaginazione is a Wordpress theme based on Bootstrap, FontAwesome, ecc.
Version: 1.0.0
Author: Officine Immaginazione
Author URI: http://www.officineimmaginazione.com
License: All right is reserved
*/

$oi_includes = array(
	'lib/utils.php',
	'lib/editor.php',
	'lib/pagination.php',
	'lib/widget.php',
	'taxonomies/category.php',
	'metaboxes/homepage.php',
    'metaboxes/category-magazine.php',
	'metaboxes/category-wine.php',
	'metaboxes/page-contacts.php',
	'metaboxes/page-institutional.php',
);

foreach ($oi_includes as $file) {
	if (!$filepath = locate_template($file)) 
		trigger_error(sprintf(__('Error locating %s for inclusion', 'roots'), $file), E_USER_ERROR);
	require_once $filepath;
}
unset($file, $filepath);

// register Designmodo_Social_Profile widget
function register_oi_profile() {
	register_widget('OI_Profile');
}

add_action('widgets_init', 'register_oi_profile');

define('PROTECTED_TERM_ID', "3");
define('CUSTOM_TAX', 'category');

function wpse158784_custom_tax_row_actions( $actions, $tag ) {
	$ids = explode(",", PROTECTED_TERM_ID);
	foreach($ids as $curid)
		if ( $tag->term_id == $curid ) unset( $actions['delete'] );
    return $actions;
}

function wpse158784_admin_footer_edit_tags_php() {
    if ( ! isset( $_GET['taxonomy'] ) || $_GET['taxonomy'] != CUSTOM_TAX ) return;
    ?>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
		var idString = "<?php echo PROTECTED_TERM_ID; ?>";
		var ids = idString.split(",");
		ids.forEach(function(id) {
			$("input#cb-select-" + id).prop('disabled', true).hide();
		});
    });
    </script>
    <?php
}

if ( is_admin() ) {
    add_filter( CUSTOM_TAX . '_row_actions', 'wpse158784_custom_tax_row_actions', 10, 2 );
    add_action( 'admin_footer-edit-tags.php', 'wpse158784_admin_footer_edit_tags_php' );
}


  
/**
 *
 * Offset the main query on our blog's home page
 *
 */
function myprefix_query_offset(&$query) {
	if(!is_admin()) {
		// Before anything else, make sure this query is on our
		// blog's home page, and that it's the main query...
		if ($query->is_category(3)) {
			// First, define your desired offset...
			if(isset($_POST["offset"]) && ($_POST["offset"] == 0) && !(is_null($_POST["offset"]))) {
				$offset = 0;
			}
			else {
				if(isset($_GET["offset"]) && ($_GET["offset"] == 0) && !(is_null($_GET["offset"]))) {
					$offset = 0;
				}
				else {
					$offset = 1;
				}
			}
				

			// Next, determine how many posts per page you want (we'll use WordPress's settings)
			$ppp = get_option('posts_per_page');

			// Next, detect and handle pagination...
			if ( $query->is_paged ) {

				// Manually determine page query offset (offset + current page (minus one) x posts per page)
				$page_offset = $offset + ( ($query->query_vars['paged']-1) * $ppp );

				// Apply adjust page offset
				$query->set('offset', $page_offset );

			}
			else {

				// This is the first page. Just use the offset...
				$query->set('offset',$offset);

			}

		} else {

			return;

		}
	}
}
add_action('pre_get_posts', 'myprefix_query_offset', 1 );

/**
 *
 * Adjust the found_posts according to our offset.
 * Used for our pagination calculation.
 *
 */
function myprefix_adjust_offset_pagination($found_posts, $query) {
	if(!is_admin()) {
		

		// Ensure we're modifying the right query object...
		if ($query->is_category(3)) {
			
			// Define our offset again...
			if(isset($_POST["offset"]) && ($_POST["offset"] == 0)) {
				$offset = 0;
			}
			else {
				if(isset($_GET["offset"]) && ($_GET["offset"] == 0)) {
					$offset = 0;
				}
				else {
					$offset = 1;
				}
			}
			
			
			// Reduce WordPress's found_posts count by the offset... 
			return $found_posts - $offset;
		}
		return $found_posts;
	}
}
add_filter('found_posts', 'myprefix_adjust_offset_pagination', 1, 2 );


add_action('template_redirect', 'redirect_category_posts');
function redirect_category_posts() {
    global $post;
    if (is_single($post->ID) && has_category(1)) {
        wp_redirect(get_category_link(1), 301);
        exit;
    }
}
 
function search_options($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
		$query->set('cat', '-1');
	}
	return $query;
}
add_filter('pre_get_posts','search_options');


/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join( $join ) {
    global $wpdb;

    if ( is_search() ) {    
        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}
add_filter('posts_join', 'cf_search_join' );

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where( $where ) {
    global $pagenow, $wpdb;

    if ( is_search() ) {
        $where = preg_replace(
            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
    }

    return $where;
}
add_filter( 'posts_where', 'cf_search_where' );

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct( $where ) {
    global $wpdb;

    if ( is_search() ) {
        return "DISTINCT";
    }

    return $where;
}
add_filter( 'posts_distinct', 'cf_search_distinct' );





function atom_search_where($where){
  global $wpdb;

  if ( is_search() )
    $where .= "OR (t.name LIKE '%".get_search_query() . "%' AND {$wpdb->posts} . post_status = 'publish')";

  return $where;
}

function atom_search_join($join){
  global $wpdb;

  if ( is_search() )
    $join .= "LEFT JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id INNER JOIN {$wpdb->term_taxonomy} tt ON tt.term_taxonomy_id=tr.term_taxonomy_id INNER JOIN {$wpdb->terms} t ON t.term_id = tt.term_id";
  return $join;
}

function atom_search_groupby($groupby){
  global $wpdb;

  // we need to group on post ID
  $groupby_id = "{$wpdb->posts} . ID";
  if ( ! is_search() || strpos($groupby, $groupby_id) !== false )
    return $groupby;

  // groupby was empty, use ours
  if ( ! strlen( trim($groupby) ) )
    return $groupby_id;

  // wasn't empty, append ours
  return $groupby . ", " . $groupby_id;
}

add_filter('posts_where', 'atom_search_where');

add_filter('posts_join', 'atom_search_join');
add_filter('posts_groupby', 'atom_search_groupby');