<?php
/*
Template Name: Pagina contatti
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<?php 
	
	$image = get_post_meta(get_the_ID(), "cd_img", true)["url"];
	
	$form = array(
		"title" => get_post_meta(get_the_ID(), "cd_title", true),
		"shortcode" => get_post_meta(get_the_ID(), "cd_shortcode", true)
	);
	
	$links = array(
		"mailto" => array(
			"txt" => get_post_meta(get_the_ID(), "cd_mailto_txt", true),
			"url" => get_post_meta(get_the_ID(), "cd_mailto_url", true)
		),
		"tel" => array(
			"txt" => get_post_meta(get_the_ID(), "cd_tel_txt", true),
			"url" => get_post_meta(get_the_ID(), "cd_tel_url", true)
		),
	);
	
	?>
	<main class="contacts-main" <?php echo $image ? 'style="background-image: url(' . $image . ')"' : ''; ?>>
		<section class="container h-100 text-center">
			<div class="row justify-content-center h-100">
				<div class="col-12 col-md-10 col-lg-8 align-self-start">
					<h1 class="contacts-title"><?php the_title(); ?></h1>
				</div>
				<div class="col-12 col-md-10 col-lg-8 col-xl-6 align-self-center">
					<div class="contacts-content"><?php the_content(); ?></div>
					<?php if(array_filter($form)) : ?>
						<?php if($form["title"]) : ?>
							<h2 class="contacts-form-title"><?php echo $form["title"]; ?></h2>
						<?php endif; ?>
						<?php if($form["shortcode"]) : ?>
							<div class="form-container"><?php echo do_shortcode($form["shortcode"]); ?></div>
						<?php endif; ?>
					<?php endif; ?>
				</div>
				<?php if(array_filter($links)) : ?>
					<div class="col-12 align-self-end big-link">
						<?php if(array_filter($links["mailto"])) : ?>
							<a href="<?php echo $links["mailto"]["url"]; ?>" ><?php echo $links["mailto"]["txt"]; ?></a>
						<?php endif; ?>
						<?php if(array_filter($links["tel"])) : ?>
							<?php if(array_filter($links["mailto"])) : ?>
								&nbsp;|&nbsp;
							<?php endif; ?>
							<a href="<?php $links["tel"]["url"]; ?>" ><?php echo $links["tel"]["txt"]; ?></a>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>
	</main>
<?php endwhile;
