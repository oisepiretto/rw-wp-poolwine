<?php
/*
Template Name: Pagina istituzionale
*/
?>

<?php while (have_posts()) : the_post(); ?>
	<?php 
	
	$prefix = "io_";
	$effectimgs = array(
		get_post_meta(get_the_ID(), $prefix . "img-0", true),
		get_post_meta(get_the_ID(), $prefix . "img-1", true),
	);
	
	//print_r(get_post_meta(get_the_ID(), $prefix . "img-0", true)); exit;
	
	$subtitle = get_post_meta(get_the_ID(), $prefix . "subtitle", true);

	$prefix = "id_";
	$description = array (
		"img" => get_post_meta(get_the_ID(), $prefix . "img", true),
		"title" => get_post_meta(get_the_ID(), $prefix . "title", true),
		"content" => get_post_meta(get_the_ID(), $prefix . "content", true),
	);
	
	$prefix = "idt_";
	$details = array (
		"img" => get_post_meta(get_the_ID(), $prefix . "img", true),
		"title" => get_post_meta(get_the_ID(), $prefix . "title", true),
		"content" => get_post_meta(get_the_ID(), $prefix . "content", true),
	);
	
	?>
	<main class="institutional-main">
		<?php if(has_post_thumbnail()) : ?>
			<section class="featured-image">
				<?php $pagetitle = get_the_title(); ?>
				<?php if($pagetitle) : ?>
					<h1 class="container featured-image-title"><?php echo $pagetitle; ?></h1>
				<?php endif; ?>
				<?php echo get_tag_picture(get_post_thumbnail_id(),"desktop-image"); ?>
                <?php echo get_tag_picture(get_post_meta(get_the_ID(), "idt_mobile-banner", true)['id'],"mobile-image");?>
				<?php  if(array_filter($effectimgs)) : ?>
					<img class="effect-image image-back" src="<?php echo $effectimgs[0]["url"]; ?>" alt="" />
					<img class="effect-image image-front" src="<?php echo $effectimgs[1]["url"]; ?>" alt="" />
				<?php endif; ?>
			</section>
		<?php endif; ?>
		<?php if(get_the_content() || $subtitle) : ?>
			<section class="container main-content">
				<div class="row justify-content-center">
					<div class="col-12 col-md-6 main-content-box">
						<?php if($subtitle) : ?>
							<h3 class="main-content-box-title"><?php echo $subtitle; ?></h3>
						<?php endif; ?>
						<div><?php the_content(); ?></div>
					</div>
				</div>
			</section>
		<?php endif; ?>
		<?php if(array_filter($description)) : ?>
			<section class="container main-description">
				<div class="row align-items-center">
					<?php if($description["img"]["id"]) : ?>
						<div class="col-12 col-md-6">
							<?php echo get_tag_picture($description["img"]["id"]); ?>
						</div>
					<?php endif; ?>
					<?php if($description["title"] || $description["content"]) : ?>
						<div class="col-12 col-md-4 offset-md-1">
							<?php if($description["title"]) : ?>
								<h2 class="main-description-title"><?php echo $description["title"]; ?></h2>
							<?php endif; ?>
							<?php if($description["content"]) : ?>
								<div class="main-description-content"><?php echo $description["content"]; ?></div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if(array_filter($details)) : ?>
			<section class="container main-details">
				<?php if($details["img"]["id"]) : ?>
					<div class="main-details-img">
						<?php echo get_tag_picture($details["img"]["id"]); ?>
					</div>
				<?php endif; ?>
				<?php if($details["title"] || $details["content"]) : ?>
					<div class="row no-gutters justify-content-center main-details-box">
						<div class="col-12 col-sm-9 col-md-6 col-lg-4">
							<?php if($details["title"]) : ?>
								<h2 class="main-details-title"><?php echo $details["title"] ?></h2>
							<?php endif; ?>
							<?php if($details["content"]) : ?>
								<div class="main-details-content"><?php echo $details["content"] ?></div>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</section>
		<?php endif; ?>
	</main>
<?php endwhile; ?>
<script>
    $(document).ready(function(){
        function IassignImageHeight() {
            win = $(window);
            if(win.width() < 576){
                $(".mobile-image").css("height",win.height());
                if((win.width()*500)/360 > 500){
                    $(".effect-image").css("width","500%");
                }else{
                    $(".effect-image").css("width",(win.width()*500)/360+"%");
                }
                
            }else if(win.width() >= 576 && win.width() < 768){
                $(".mobile-image").css("height",win.height());
                if(((win.width()*500)/360)/2 > 300){
                    $(".effect-image").css("width","300%");
                }else{
                    $(".effect-image").css("width",((win.width()*500)/360)/2+"%");
                }
            }else{
                $(".effect-image").css("width","120%");
            }
        }
        IassignImageHeight();
        $(window).resize(IassignImageHeight);
    });
</script>