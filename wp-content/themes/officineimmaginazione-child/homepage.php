<?php
/*
Template Name: Homepage
*/
?>
<?php while (have_posts()) : the_post(); ?>
	<main class="homepage-main">
		<?php if(has_post_thumbnail()) : ?>
			<section class="featured-image homepage-featured-image">
				<?php echo get_tag_picture(get_post_thumbnail_id(),"desktop-image"); ?>
                <?php echo get_tag_picture(get_post_meta(get_the_ID(), "id_mobile-banner", true)['id'],"mobile-image");?>
				<div class="container h-100">
					<div class="row h-100 align-items-center">
						<div class="col-12 col-md-6 col-lg-9 animated-img-box">
							<?php for($i = 1; $i <= 6; $i++) : ?>
								<img class="img-fluid img-<?php echo $i; ?>" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/animation/{$i}.png"; ?>" alt="" />
							<?php endfor; ?>
						</div>
						<div class="col-12 col-md-6 col-lg-3 homepage-intro-content">
							<h1 class="homepage-title"><?php the_title(); ?></h1>
							<?php 
							$prefix = "hi_";
							$intro = array(
								"subtitle" => get_post_meta(get_the_ID(), $prefix . "subtitle", true),
								"img" => get_post_meta(get_the_ID(), $prefix . "img", true)["id"],
								"link" => array(
									"txt" => get_post_meta(get_the_ID(), $prefix . "btn_txt", true),
									"url" => get_post_meta(get_the_ID(), $prefix . "btn_url", true)
								)
							);
							?>
							<?php if(array_filter($intro)) : ?>
								<?php if($intro["subtitle"]) : ?>
									<h4 class="homepage-subtitle"><?php echo $intro["subtitle"]; ?></h4>
								<?php endif; ?>
								<?php if($intro["img"]) : ?>
									<?php echo get_tag_picture($intro["img"]); ?>
								<?php endif; ?>
								<?php if(array_filter($intro["link"])) : ?>
									<a class="btn btn-inverted" href="<?php echo $intro["link"]["url"]; ?>"><?php echo $intro["link"]["txt"]; ?></a>
								<?php endif; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</section>
		<?php endif; ?>
		<?php 
		$catid = icl_object_id(3, 'category', false);
		$args = array(
			"posts_per_page"	=> 1,
			'tax_query'			=> array (
				array (
					"taxonomy" => "category",
					"field" => "id",
					"terms" => $catid
				)
			),
		);
		$wine = new WP_Query($args); 
		?>
		<?php if($wine->have_posts()) : ?> 
			<section class="container category-wine-intro">
				<?php while($wine->have_posts()) : $wine->the_post(); ?>
					<article <?php post_class("row align-items-center wine"); ?> id="wine-<?php the_ID(); ?>">
						<div class="col-12 col-md-3">
							<h3 class="h1 category-wine-title"><?php echo get_cat_name($catid); ?></h3>
							<h5 class="category-wine-date"><?php echo get_the_date("d F Y"); ?></h5>
							<hr />
							<div class="wine-title-container">
								<h3 class="wine-title"><?php the_title(); ?></h3>
								<?php 
								$type = get_post_meta(get_the_ID(), 'wd_type', true);
								$productor = get_post_meta(get_the_ID(), 'wd_productor', true); 
								?>
								<?php if($type) : ?>
									<p class="wine-type"><?php echo $type; ?></p>
								<?php endif; ?>
							</div>
							<div class="wine-productor-container">
								<?php if($productor) : ?>
									<h4 class="wine-productor-title"><?php _e("Produttore", "officineimmaginazione"); ?></h4>
									<p class="wine-productor-desc"><?php echo $productor; ?></p>
								<?php endif; ?>
							</div>
							<a class="btn btn-default" href="<?php the_permalink(); ?>"><?php _e("Scopri", "officineimmaginazione"); ?></a>
						</div>
						<div class="col-12 col-md-6">
							<div class="wine-img-container">
								<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
							</div>
						</div>
						<div class="col-12 col-md-3">
							<?php if(get_the_content()) : ?>
								<div class="wine-excerpt-container">
									<h5 class="wine-intro-title"><?php _e("Descrizione", "officineimmaginazione"); ?></h5>
									<div class="wine-excerpt"><?php echo wp_trim_words(get_the_content(), 20); ?></div>
								</div>
							<?php endif; ?>
							<?php 
							$foods = wp_get_post_terms(get_the_ID(), "post_food"); 
							$song = array(
								"artist"	=> get_post_meta(get_the_ID(), 'wd_artist', true),
								"title"		=> get_post_meta(get_the_ID(), 'wd_song_title', true),
								"url"		=> get_post_meta(get_the_ID(), 'wd_song_url', true),
							);
							?>
							<?php if(array_filter($foods) || array_filter($song)) : ?>
								<div class="wine-match">
									<h5 class="wine-intro-title"><?php _e("Abbinamenti", "officineimmaginazione"); ?></h5>
									<?php if(array_filter($foods)) : ?>
										<span class="d-block mt-3 mb-1"><?php _e("Food", "officineimmaginazione"); ?></span>
										<div class="d-flex align-items-center">
											<?php foreach($foods as $i => $food) : ?>
												<?php $food_img = get_option("post_food_" . $food->term_id)["image"]; ?>
												<?php if($i) : ?>
													<span class="plus">+</span>
												<?php endif; ?>
												<?php if($food_img) : ?>
													<img class="wine-icon" src="<?php echo $food_img; ?>" alt="<?php echo $food->name; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $food->name; ?>" />
												<?php else : ?>
													<span><?php echo $food->name; ?></span>
												<?php endif; ?>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
									<?php if(array_filter($song)) : ?>
										<span class="d-block mt-3 mb-1"><?php _e("Music", "officineimmaginazione"); ?></span>
										<a class="d-flex align-items-center wine-song" href="<?php echo $song["url"]; ?>" target="_blank">
											<div>
												<img class="wine-icon" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/music-icon.svg"; ?>" alt="" />
											</div>
											<div>
												<?php if($song["artist"]) : ?>
													<strong class="d-block"><?php echo $song["artist"]; ?></strong>
												<?php endif; ?>
												<?php if($song["title"]) : ?>
													<span class="d-block"><?php echo $song["title"]; ?></span>
												<?php endif; ?>
											</div>
											<div>
												<i class="fas fa-play"></i>
											</div>
										</a>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</article>
				<?php endwhile;?>
			</section>
		<?php endif; ?>
		<?php 
		$catid = icl_object_id(1, 'category', false);
		$args = array(
			'posts_per_page'   => 10,
			'category'         => $catid,
			'orderby'          => 'date',
			'order'            => 'DESC',
			'post_type'        => 'post',
			'post_status'      => 'publish',
			'suppress_filters' => false,
		);
		$magazines = get_posts($args);
		?>
		<?php if (count($magazines)) : ?>
			<section class="container magazine-carousel">
				<h2 class="magazine-carousel-title"><?php echo get_cat_name($catid); ?></h2>
				<div class="magazine-carousel-row">
					<?php foreach ($magazines as $magazine) : ?>
						<?php $file = get_post_meta($magazine->ID, 'magazine_pdf', true); ?>
						<article class="magazine" id="magazine-<?php echo $magazine->ID; ?>">
							<a href="<?php echo $file["url"] ? $file["url"] : "javascript:void(0)" ?>" target="_blank">
								<div class="magazine-img-container">
									<?php echo get_tag_picture(get_post_thumbnail_id($magazine->ID)); ?>
								</div>
								<h5 class="magazine-title"><?php echo $magazine->post_title; ?></h5>
								<p class="magazine-date"><?php echo date_i18n('d F', strtotime($magazine->post_date)); ?></p>
							</a>
						</article>
					<?php endforeach; ?>
					<?php if (count($magazines) <= 3) : ?>
						<?php for($k = 1; $k <= (3-count($magazines)); $k++) : ?>
							<article class="magazine" id="magazine-<?php echo $magazine->ID; ?>">
								<a class="disabled" href="javascript:void(0)">
									<div class="magazine-img-container">
										<img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/magazine-empty.jpg" alt="Empty magazine" />
									</div>
									<h5 class="magazine-title"><?php _e("In arrivo...", "officineimmaginazione"); ?></h5>
									<p class="magazine-date"><?php _e("Rimani connesso", "officineimmaginazione"); ?></p>
								</a>
							</article>
						<?php endfor; ?>
					<?php endif; ?>
				</div>
				<div class="magazine-carousel-btn">
					<a class="btn btn-default" href="<?php echo get_category_link($catid); ?>"><?php _e("Tutte le edizioni", "officineimmaginazione"); ?></a>
				</div>
			</section>
		<?php endif; ?>
		<?php wp_reset_query(); ?>
		<?php 
		$prefix = "id_";
		$details = array(
			"imgLeft" => get_post_meta(get_the_ID(), $prefix . "img-0", true)["id"],
			"imgRight" => get_post_meta(get_the_ID(), $prefix . "img-1", true)["id"],
			"title" => get_post_meta(get_the_ID(), $prefix . "title", true),
			"content" => get_post_meta(get_the_ID(), $prefix . "content", true),
			"link" => array(
				"txt" => get_post_meta(get_the_ID(), $prefix . "btn_txt", true),
				"url" => get_post_meta(get_the_ID(), $prefix . "btn_url", true)
			)
		);
		?>
		<?php if(array_filter($details)) : ?>
			<section class="container homepage-main-details">
				<div class="row align-items-center">
					<?php if($details["imgLeft"] || $details["imgRight"]) : ?>
						<div class="col-12 col-md-7 details-img-container">
							<?php if($details["imgLeft"]) : ?>
								<?php echo get_tag_picture($details["imgLeft"]); ?>
							<?php endif; ?>
							<?php if($details["imgRight"]) : ?>
								<?php echo get_tag_picture($details["imgRight"]); ?>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					<?php if($details["title"] || $details["content"] || array_filter($details["link"])) : ?>
						<div class="col-12 col-md-5">
							<?php if($details["title"]) : ?>
								<h2 class="details-title"><?php echo $details["title"]; ?></h2>
							<?php endif; ?>
							<?php if($details["content"]) : ?>
								<div><?php echo $details["content"]; ?></div>
							<?php endif; ?>
							<?php if(array_filter($details["link"])) : ?>
								<div>
									<a class="btn btn-default" href="<?php echo $details["link"]["url"]; ?>"><?php echo $details["link"]["txt"]; ?></a>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>
			</section>
		<?php endif; ?>
	</main>
<?php endwhile; ?>

<script>
	jQuery(document).ready(function($){
		
		$('[data-toggle="tooltip"]').tooltip();
		
        $(".magazine-carousel-row").mCustomScrollbar({
            theme:"my-theme",
            axis:"x",
			scrollbarPosition: "outside",
			alwaysShowScrollbar: 1,
			setTop: "-32px",
            mouseWheel:{ axis: "x" }
        });
		
		var currentMousePos = { x: -1, y: -1 };
		$(document).mousemove(function(event) {
			if ($(window).width() > 768){
				var wWidth = $(".animated-img-box").width();
				var wHeight = $(".animated-img-box").height();
				var centerX = wWidth / 2;
				var centerY = wHeight / 2;
				for(i = 1; i <= 6; i++) {
					yBgPosition = ((centerY - event.pageY) / (i * 10));
					xBgPosition = ((centerX - event.pageX) / (i * 10));
					$(".animated-img-box .img-" + i).css('left', xBgPosition + 'px');
					$(".animated-img-box .img-" + i).css('top', "calc(50% - " + yBgPosition + 'px)');
				}
			}
		});
    });
</script>

