<?php while (have_posts()) : the_post(); ?>
	<article class="container">
		<div class="row">
			<main class="col-12 col-md-8 post-wine-main">
				<section class="wine-header">
					<a class="btn btn-default" href="<?php echo get_category_link(3); ?>"><?php _e("Torna indietro", "officineimmaginazione"); ?></a>
					<div class="wine-title-box">
						<h4 class="wine-date"><?php echo get_the_date("d F Y"); ?></h4>
						<h1 class="wine-title">
							<span class="d-block"><?php the_title(); ?></span>
							<?php $productor = get_post_meta(get_the_ID(), 'wd_productor', true); ?>
							<?php if($productor) : ?>
								<span class="d-block"><?php echo $productor; ?></span>
							<?php endif; ?>
						</h1>
					</div>
				</section>
				<section>
					<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
					<div class="share-box">
						<span><?php _e("Condividi", "officineimmaginazione"); ?></span>
						<span id="share-<?php echo get_the_ID() ?>"></span>
					</div>
				</section>
				<section class="row">
					<div class="col-12 col-md-6">
						<h5 class="wine-content-title"><?php _e("Descrizione", "officineimmaginazione"); ?></h5>
						<?php the_content(); ?>
					</div>
					<div class="col-12 col-md-6">
						<?php 
						$foods = wp_get_post_terms(get_the_ID(), "post_food");
						$location = get_post_meta(get_the_ID(), 'wd_location', true);
						$song = array(
							"artist"	=> get_post_meta(get_the_ID(), 'wd_artist', true),
							"title"		=> get_post_meta(get_the_ID(), 'wd_song_title', true),
							"url"		=> get_post_meta(get_the_ID(), 'wd_song_url', true),
						);
						?>
						<?php if($location || array_filter($foods) || array_filter($song)) : ?>
							<div class="wine-match">
								<?php if($location) : ?>
									<h5 class="wine-content-title"><?php _e("Location", "officineimmaginazione"); ?></h5>
									<div class="mb-4"><?php echo $location; ?></div>
								<?php endif; ?>
								<h5 class="wine-content-title"><?php _e("Abbinamenti", "officineimmaginazione"); ?></h5>
								<?php if(array_filter($foods)) : ?>
									<span class="d-block mt-3 mb-1"><?php _e("Food", "officineimmaginazione"); ?></span>
									<div class="d-flex align-items-center">
										<?php foreach($foods as $i => $food) : ?>
											<?php $food_img = get_option("post_food_" . $food->term_id)["image"]; ?>
											<?php if($i) : ?>
												<span class="plus">+</span>
											<?php endif; ?>
											<?php if($food_img) : ?>
												<img class="wine-icon" src="<?php echo $food_img; ?>" alt="<?php echo $food->name; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $food->name; ?>" />
											<?php else : ?>
												<span><?php echo $food->name; ?></span>
											<?php endif; ?>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
								<?php if(array_filter($song)) : ?>
									<span class="d-block mt-3 mb-1"><?php _e("Music", "officineimmaginazione"); ?></span>
									<a class="d-flex align-items-center wine-song" href="<?php echo $song["url"]; ?>" target="_blank">
										<div>
											<img class="wine-icon" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/music-icon.svg"; ?>" alt="" />
										</div>
										<div>
											<?php if($song["artist"]) : ?>
												<strong class="d-block"><?php echo $song["artist"]; ?></strong>
											<?php endif; ?>
											<?php if($song["title"]) : ?>
												<span class="d-block"><?php echo $song["title"]; ?></span>
											<?php endif; ?>
										</div>
										<div>
											<i class="fas fa-play"></i>
										</div>
									</a>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
				</section>
			</main>
			<?php if(is_active_sidebar("sidebar")) : ?>
				<aside class="col-12 col-md-4 sidebar">
					<?php do_shortcode(dynamic_sidebar("sidebar")); ?>
				</aside>
			<?php endif; ?>
		</div> 
	</article>
	<script async type="text/javascript">
		jQuery(document).ready(function($) {
			$('[data-toggle="tooltip"]').tooltip();
            if($(window).width() > 768){
                $(".sidebar").css("padding-top", $(".wine-header").outerHeight() + "px");
            }else{
                $(".sidebar").css("padding-top", 30);
            }
            
            $(window).resize(function(){
                if($(window).width() > 768){
                    $(".sidebar").css("padding-top", $(".wine-header").outerHeight() + "px");
                }else{
                    $(".sidebar").css("padding-top", 30);
                }
            });
			
			$("#share-<?php echo get_the_ID() ?>").jsSocials({
				url: "<?php the_permalink(); ?>",
				showLabel: false,
				showCount: false,
				shares: [
					{share: "facebook", logo: "fab fa-facebook"},
					{share: "pinterest", logo: "fab fa-pinterest"}
				]
			});
            
            
            
		});
	</script>
<?php endwhile;