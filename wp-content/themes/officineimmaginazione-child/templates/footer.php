<footer class="container">
	<div class="row">
		<?php dynamic_sidebar("footer"); ?>
	</div>
</footer>

<script>
	jQuery(document).ready(function($) {
		$(".navbar-toggler").click(function(){
			$("body").toggleClass("no-scroll");
		});
        
        $(".mobile-search").on("click",function(){
            $(".mobile-search-form").addClass("open");
            $(".mobile-search-form .search-submit i").css("display","block");
            $(".mobile-search-form .fa-search").addClass("fa-lg");
        });

        $(".form-closer").on("click",function(){
            $(".mobile-search-form").removeClass("open");
            $(".mobile-search-form .search-submit i").css("display","none");
        });
        
        $(".mobile-search").css("color","black");
        
        
        $(".navbar-toggler").on("click",function(){
            $(".mobile-search-form").removeClass("open");
        });
        
        
	});
</script>

<?php if(has_post_thumbnail() && is_page()) : ?>
	<script>
		jQuery(document).ready(function($) {
			function assignImageHeight() {
				var iheight = $(window).height();
                $(".featured-image").height(iheight);
                win = $(window);
                if(win.width() < 768){
                    var bannerH = $(".homepage-featured-image").height();
                    var contentH = $(".homepage-intro-content").height();
                    if((bannerH-contentH) < $(".mobile-image").width()){
                        $(".mobile-image").css("width",bannerH-contentH);
                    }
                    else{
                        $(".mobile-image").css("height",bannerH-contentH);
                        $(".mobile-image").css("width","auto");
                    }
                }
			}
			assignImageHeight();
			$(window).resize(assignImageHeight);
            $(".mobile-search").css("color","white");
		});
	</script>
<?php endif; ?>

<?php if(is_category(3)) : ?>
	
<?php endif;