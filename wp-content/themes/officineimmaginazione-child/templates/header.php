<?php
$headerclass = "header-white";
$logo = get_stylesheet_directory_uri() . "/assets/img/logo.svg";
if(!is_archive() && !is_single() && has_post_thumbnail() && !is_search()) {
	$headerclass = "header-transparent";
	$logo = get_stylesheet_directory_uri() . "/assets/img/logo-white.svg";
}
?>
<div class="d-flex align-items-center justify-content-center mobile-search-form">
    <?php get_search_form(); ?>
    <button class="form-closer" id="navbar-closer" type="button">
        <img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/close-black.png" alt="Close navbar" />
    </button>
</div>
<header class="header <?php echo $headerclass; ?>">
	<nav class="navbar">
		<div class="container">
			<a class="navbar-brand order-md-0" href="<?php echo esc_url(home_url('/')); ?>">
				<img class="img-fluid img-logo" src="<?php echo $logo; ?>" alt="" />
			</a>
            <button class="mobile-search" type="button">
                <i class="fas fa-search fa-lg"></i>
            </button>
			<button class="navbar-toggler mx-2 mx-md-3 order-md-2" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
				<span></span>
				<span></span>
				<span></span>
			</button>
			<div class="order-md-3 dropdown language-menu">
				<a class="dropdown-toggle" href="javascript:void(0)" data-toggle="dropdown"><?php echo ICL_LANGUAGE_CODE ?> <i class="fas fa-angle-down"></i></a>
				<ul class="dropdown-menu dropdown-menu-center">
					<?php foreach(icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str') as $lang) : ?>
						<?php if(ICL_LANGUAGE_CODE != $lang["code"]) : ?>
							<li><a class="dropdown-item" href="<?php echo $lang["url"]; ?>"><?php echo $lang["code"]; ?></a></li>
						<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</div>
			<?php get_search_form(); ?>
			<div class="collapse navbar-collapse" id="navbarToggler">
				<div class="d-flex h-100 align-content-center flex-wrap">
					<div class="navbar-collapse-logo">
						<img src="<?php echo get_stylesheet_directory_uri() . "/assets/img/logo-min.svg"; ?>" alt="" />
					</div>
					<?php
					if (has_nav_menu('primary_navigation')) {
						wp_nav_menu(array(
							'theme_location' => 'primary_navigation', 
							'menu_class' => 'nav navbar-nav navbar-right navbar-menu'
						));
					}
					?>
				</div>
			</div>
		</div>
	</nav>
</header>