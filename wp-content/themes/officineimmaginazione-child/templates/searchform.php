<form class="form-inline ml-md-auto search-form order-md-1" role="search" method="get" action="<?php echo esc_url(home_url('/')); ?>">
	<div class="input-group">
		<input class="form-control search-field" name="s" type="search" value="<?php echo get_search_query(); ?>"  placeholder="<?php _e('Cerca vino', 'officineimmaginazione'); ?>" aria-label="Search" data-swplive="true">
		<div class="input-group-prepend">
			<button type="submit" class="btn btn-search search-submit"><i class="fas fa-search"></i></button>
		</div>
	</div>
</form>