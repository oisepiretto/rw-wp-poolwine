<?php 
/*
Theme Name: Officine Immaginazione
Theme URI: http://www.officineimmaginazione.com/wordpress-themes
Description: OfficineImmaginazione is a Wordpress theme based on Bootstrap, FontAwesome, ecc.
Version: 1.0.0
Author: Officine Immaginazione
Author URI: http://www.officineimmaginazione.com
License: All right is reserved
*/

/**
 * Create Food Tax Typology
 */
add_action('init', 'createPostFood');
function createPostFood() {
	$labels = array(
		'name'				=> __('Cibi', 'officineimmaginazione'),
		'singular_name'     => __('Cibo', 'officineimmaginazione'),
		'add_new_item'      => __('Aggiungi Cibo', 'officineimmaginazione'),
		'edit_item'			=> __('Modifica Cibo', 'officineimmaginazione'),
		'new_item_name'     => __('Nuovo Cibo', 'officineimmaginazione'),
		'all_items'			=> __('Tutti i Cibi', 'officineimmaginazione'),
		'search_items'		=> __('Cerca Cibo', 'officineimmaginazione'),
		'update_item'		=> __('Aggiorna Cibo', 'officineimmaginazione'),
	);
	$args = array(
		'labels'					=> $labels,
		'hierarchical'				=> false,
		'query_var'					=> true,
		'show_admin_column'			=> true,
		'update_count_callback'		=> '_update_post_term_count',
		'rewrite'					=> array('slug'	=> 'post_food') //rewrite the tax permalink structure
	);
	register_taxonomy('post_food', 'post', $args);
}


/**
 * Create Music Genres Tax Typology
 */
add_action('init', 'createPostMusicGenre');
function createPostMusicGenre() {
	$labels = array(
		'name'				=> __('Generi musicali', 'officineimmaginazione'),
		'singular_name'     => __('Genere musicale', 'officineimmaginazione'),
		'add_new_item'      => __('Aggiungi Genere musicale', 'officineimmaginazione'),
		'edit_item'			=> __('Modifica Genere musicale', 'officineimmaginazione'),
		'new_item_name'     => __('Nuovo Genere musicale', 'officineimmaginazione'),
		'all_items'			=> __('Tutti i Generi musicali', 'officineimmaginazione'),
		'search_items'		=> __('Cerca Genere musicale', 'officineimmaginazione'),
		'update_item'		=> __('Aggiorna Genere musicale', 'officineimmaginazione'),
	);
	$args = array(
		'labels'					=> $labels,
		'hierarchical'				=> false,
		'query_var'					=> true,
		'show_admin_column'			=> true,
		'update_count_callback'		=> '_update_post_term_count',
		'rewrite'					=> array('slug'	=> 'post_music_genre') //rewrite the tax permalink structure
	);
	register_taxonomy('post_music_genre', 'post', $args);
}

// Add Upload fields to "Add New Taxonomy" form
function add_post_food_image_field() {
	// this will add the custom meta field to the add new term page
	?>
	<div class="form-field">
		<label for="post_food_image"><?php _e('Series Image:', 'officineimmaginazione'); ?></label>
		<input type="text" name="post_food_image[image]" id="post_food_image[image]" class="post_food-image hidden" value="<?php echo $post_foodimage; ?>">
		<input class="upload_image_button button" name="_add_post_food_image" id="_add_post_food_image" type="button" value="Select/Upload Image" />
		<script>
			jQuery(document).ready(function() {
				jQuery('#_add_post_food_image').click(function() {
					wp.media.editor.send.attachment = function(props, attachment) {
						jQuery('.post_food-image').val(attachment.url);
					}
					wp.media.editor.open(this);
					return false;
				});
			});
		</script>
	</div>
	<?php
}
add_action( 'post_food_add_form_fields', 'add_post_food_image_field', 10, 2 );

// Add Upload fields to "Edit Taxonomy" form
function post_food_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "post_food_$t_id" ); ?>
	
	<tr class="form-field">
	<th scope="row" valign="top"><label for="_post_food_image"><?php _e('Series Image', 'officineimmaginazione'); ?></label></th>
		<td>
			<?php
				$post_foodimage = esc_attr( $term_meta['image'] ) ? esc_attr( $term_meta['image'] ) : ''; 
				?>
			<input type="text" name="post_food_image[image]" id="post_food_image[image]" class="post_food-image hidden" value="<?php echo $post_foodimage; ?>">
			<input class="upload_image_button button" name="_post_food_image" id="_post_food_image" type="button" value="Select/Upload Image" />
		</td>
	</tr>
	<tr class="form-field">
	<th scope="row" valign="top"></th>
		<td style="height: 150px;">
			<style>
				div.img-wrap {
					background-repeat: no-repeat;
					background-position: center; 
					background-size: contain; 
					max-width: 100px; 
					max-height: 100px; 
					width: 100%; 
					height: 100%; 
					overflow:hidden; 
				}
				div.img-wrap img {
					max-width: 450px;
				}
			</style>
			<div class="img-wrap">
				<img src="<?php echo $post_foodimage; ?>" id="post_food-img">
			</div>
			<script>
			jQuery(document).ready(function() {
				jQuery('#_post_food_image').click(function() {
					wp.media.editor.send.attachment = function(props, attachment) {
						jQuery('#post_food-img').attr("src",attachment.url)
						jQuery('.post_food-image').val(attachment.url)
					}
					wp.media.editor.open(this);
					return false;
				});
			});
			</script>
		</td>
	</tr>
<?php
}
add_action( 'post_food_edit_form_fields', 'post_food_edit_meta_field', 10, 2 );

// Save Taxonomy Image fields callback function.
function save_post_food_custom_meta( $term_id ) {
	if (isset( $_POST['post_food_image'])) {
		$t_id = $term_id;
		$term_meta = get_option( "post_food_$t_id" );
		$cat_keys = array_keys( $_POST['post_food_image'] );
		foreach ($cat_keys as $key) {
			if (isset ( $_POST['post_food_image'][$key])) {
				$term_meta[$key] = $_POST['post_food_image'][$key];
			}
		}
		// Save the option array.
		update_option("post_food_$t_id", $term_meta);
	}
}  
add_action( 'edited_post_food', 'save_post_food_custom_meta', 10, 2 );  
add_action( 'create_post_food', 'save_post_food_custom_meta', 10, 2 );