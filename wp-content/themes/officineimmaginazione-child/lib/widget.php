<?php
/*
  Plugin Name: Officine Immaginazione Child User Profile Widget
  Plugin URI: http://www.officineimmaginazione.com
  Description: 
  Author: Officine Immaginazione
  Author URI: https://www.officineimmaginazione.com
*/

class OI_Profile extends WP_Widget {
	
	function __construct() {
		parent::__construct(
			'OI_Profile',
			__('Profilo descrittivo', 'officineimmaginazione'),
			array ('description' => __('Profilo descrittivo dell\'utente', 'officineimmaginazione'))
		);
	}
	
	public function form($instance) {
        isset($instance['title']) ? $title = $instance['title'] : null;
        isset($instance['description']) ? $description = $instance['description'] : null;
        isset($instance['img']) ? $img = $instance['img'] : null;
		isset($instance['link_txt']) ? $link_txt = $instance['link_txt'] : null;
        isset($instance['link_url']) ? $link_url = $instance['link_url'] : null;
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Titolo:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Descrizione:'); ?></label>
			<textarea class="widefat" id="<?php echo $this->get_field_id('description'); ?>" name="<?php echo $this->get_field_name('description'); ?>" rows="4"><?php echo esc_attr($description); ?></textarea>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('img'); ?>"><?php _e('Immagine:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('img'); ?>" name="<?php echo $this->get_field_name('img'); ?>" type="text" value="<?php echo esc_attr($img); ?>">
        </p>
		<p>
            <label for="<?php echo $this->get_field_id('link_txt'); ?>"><?php _e('Testo Pulsante:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_txt'); ?>" name="<?php echo $this->get_field_name('link_txt'); ?>" type="text" value="<?php echo esc_attr($link_txt); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('link_url'); ?>"><?php _e('Url Pulsante:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('link_url'); ?>" name="<?php echo $this->get_field_name('link_url'); ?>" type="text" value="<?php echo esc_attr($link_url); ?>">
        </p>
        <?php
    }
	
	public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['description'] = (!empty($new_instance['description']) ) ? strip_tags($new_instance['description']) : '';
        $instance['img'] = (!empty($new_instance['img']) ) ? strip_tags($new_instance['img']) : '';
		$instance['link_txt'] = (!empty($new_instance['link_txt']) ) ? strip_tags($new_instance['link_txt']) : '';
        $instance['link_url'] = (!empty($new_instance['link_url']) ) ? strip_tags($new_instance['link_url']) : '';
        return $instance;
    }
	
	public function widget($args, $instance) {
		$title = $instance['title'];
		$description = $instance['description'];
		$img = $instance['img'];
		$link_txt = $instance['link_txt'];
		$link_url = $instance['link_url'];

		// social profile link
		$title_profile = '<p class="title"><strong>' . $title . '</strong></p>';
		$description_profile = '<p class="description">' . $description . '</p>';
		$img_profile = '<img class="img-fluid" src="' . $img . '" alt="" />';
		$btn = '<a class="btn btn-default" href="' . $link_url . '" target="_blank">' . $link_txt . '</a>';
		

		echo $args['before_widget'];
		echo '<div class="profile-box">';
		echo (!empty($img)) ? $img_profile : null;
		echo (!empty($title)) ? $title_profile : null;
		echo (!empty($description)) ? $description_profile : null;
		echo (!empty($link_txt) && !empty($link_url)) ? $btn : null;
		echo '</div>';
		echo $args['after_widget'];
	}
	
	
	
}