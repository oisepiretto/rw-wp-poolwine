<?php

/*
 * Creates a custom colour palette
 */
function dmw_custom_palette( $init ) { 
  $custom_colours = '"000000", "Nero", "ffffff", "Bianco", "365ec1", "Cerulean Blue", "f6c71a", "Moon Yellow"'; 
  $init['textcolor_map'] = '['.$custom_colours.']'; 
  return $init; 
} 
add_filter('tiny_mce_before_init', 'dmw_custom_palette'); 