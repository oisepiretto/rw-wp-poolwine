<?php
/**
 * Utility functions
 */
function is_element_empty($element) {
  $element = trim($element);
  return !empty($element);
}

// Tell WordPress to use searchform.php from the templates/ directory
function roots_get_search_form($form) {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', 'roots_get_search_form');

/**
 * Add page slug to body_class() classes if it doesn't exist
 */
function roots_body_class($classes) {
  // Add post/page slug
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }
  return $classes;
}
add_filter('body_class', 'roots_body_class');

/* Disable comments on media attachmets */
function filter_media_comment_status($open, $postId) {
	$post = get_post($postId);
	if($post->post_type == 'attachment') {
		return false;
	}
	return $open;
}

/*
 * Hide editor on specific post types.
 */
/*add_action('admin_init', 'hideEditor');
function hideEditor() {
	// Get the Post ID.
	$postId = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if(!isset($postId)) return;
	// Hide the editor on a page with a specific page template
	// Get the name of the Page Template file.
	$templateFile = get_post_meta($postId, '_wp_page_template', true);
	if($templateFile == 'page-custom.php'){ // the filename of the page template
		remove_post_type_support('page', 'editor');
	}
}*/

function covert_youtube_embed($youTubeLink) {
  return preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i", "<iframe class=\"full-width\" height=\"600\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>", $youTubeLink);
}


function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');


/**
 * Reset delle dimensioni delle immagini generate
 */
function prefix_remove_default_images($sizes) {
    unset( $sizes['thumbnail']); // 150px
    unset( $sizes['medium']); // 300px
    unset( $sizes['large']); // 1024px
    unset( $sizes['medium_large']); // 768px
    return $sizes;
}

/**
 * Genera nuove dimensioni per le immagini
 */
function set_image_sizes($to_add){
    if(is_array($to_add))
        foreach ($to_add as $add_size) 
            add_image_size($add_size[0], $add_size[1], $add_size[2]);
}

/**
 * Genera un tag picture
 */
function get_tag_picture($img_id,$class=""){
    echo '<picture>'
        . '<source media="(max-width: 575px)" srcset="' . wp_get_attachment_image_src($img_id, "xs")[0] . '">'
        . '<source media="(min-width: 576px) and (max-width: 767px)" srcset="' . wp_get_attachment_image_src($img_id, "sm")[0] . '">'
        . '<source media="(min-width: 768px) and (max-width: 991px)" srcset="' . wp_get_attachment_image_src($img_id, "md")[0] . '">'
        . '<source media="(min-width: 992px) and (max-width: 1199px)" srcset="' . wp_get_attachment_image_src($img_id, "lg")[0] . '">'
        . wp_get_attachment_image($img_id, "full", 0, array("class" => "img-fluid ".$class))
        . '</picture>';
}