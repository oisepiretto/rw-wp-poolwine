<?php 
$catid = get_query_var("cat");


if(isset($_POST["cid"]) || isset($_GET["cid"])) {
	if($_POST["cid"] || $_GET["cid"]) {
		$cid = $_POST["cid"] ? $_POST["cid"] : $_GET["cid"];
	}
}

if(isset($_POST["year"]) || isset($_GET["year"])) {
	if($_POST["year"] || $_GET["year"]) {
		$year = $_POST["year"] ? $_POST["year"] : $_GET["year"];
	}
}

if(isset($_POST["month"]) || isset($_GET["month"])) {
	if($_POST["month"] || $_GET["month"]) {
		$month = $_POST["month"] ? $_POST["month"] : $_GET["month"];
	}
}

?>
<section class="filter-bar" id="filter-bar">
	<div class="container">
		<div class="row no-gutters">
			<div class="d-flex align-items-center filter-bar-title"><?php _e("Filtra per:", "officineimmaginazione") ?></div>
			<div class="filter-bar-content">
				<div>
					<?php
					if($terms = get_terms('category', array( 'parent' => $catid))) : // to make it simple I use default categories
						echo '<select class="custom-select filter-select" id="cfilter" name="category_filter"><option selected disabled>' . __("Edizione", "officineimmaginazione") . '</option>';
						foreach ($terms as $term) {
							$selected = ($term->term_id == $fid) ? 'selected' : '';
							echo '<option value="' . $term->term_id . '"' . $selected . '>' . $term->name . '</option>'; // ID of the category as the value of an option
						}
						echo '</select>';
					endif;
					?>
				</div>
				<div>
					<?php 
					$yargs = array(
						"posts_per_page" => -1,
						"post_status" => "publish",
						"oderby"	=> "date",
						"order" => "ASC"
					);
					$oldest_year = (int)date("Y", strtotime(get_posts($yargs)[0]->post_date));
					$current_year = (int)date("Y");
					?>
					<select class="custom-select filter-select" id="yfilter" name="year_filter">
						<option selected disabled><?php _e("Anno", "officineimmaginazione"); ?></option>
						<?php for ($y = $oldest_year; $y <= $current_year; $y++) : ?>
							<option value="<?php echo $y; ?>" <?php echo ($y == $year) ? "selected" : ""; ?>><?php echo $y; ?></option>
						<?php endfor; ?>
					</select>
				</div>
				<div>
					<select class="custom-select filter-select" id="mfilter" name="month_filter">
						<option selected disabled><?php _e("Mese", "officineimmaginazione"); ?></option>
						<?php for ($m = 1; $m <= 12; $m++) : ?>
							<option value="<?php echo $m; ?>" <?php echo ($m == $month) ? "selected" : ""; ?>><?php echo date_i18n('F', mktime(0, 0, 0, $m, 10)); ?></option>
						<?php endfor; ?>
					</select>
				</div>
				<div>
					<button class="btn btn-default btn-remove-filters"><?php _e("Azzera", "officineimmaginazione"); ?></button>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="container">
	<h1 class="category-magazine-title"><?php echo get_cat_name($catid); ?></h1>
</section>
<section class="container">
	<div class="row">
		<main class="col-12 col-md-8 category-magazine-main" id="category-magazine-main">
			<div id="content">
				
					<?php
					//wp_reset_query();
					$args = array(
						'post_type' 		=> 'post',
						'post_status' 		=> 'publish',
						'orderby'			=> 'date',
						'order'				=> 'DESC',
						'paged'				=> get_query_var('paged') ? get_query_var('paged') : 1,
						'suppress_filters'	=> false,
						'tax_query' => array(
							array(
								'taxonomy' 	=> 'category',
								'field' 	=> 'id',
								'terms' 	=> $catid
							)
						)
					);

					if($_POST["cid"] || $_GET["cid"]) {
						$tmp = $_POST["cid"] ? $_POST["cid"] : $_GET["cid"];
						$args["tax_query"][1] = array(
							"taxonomy" => "category",
							"field" => "id",
							"terms" => $tmp
						);
					}

					if($_POST["year"] || $_GET["year"]) {
						$tmp = $_POST["year"] ? $_POST["year"] : $_GET["year"];
						$args["date_query"]["year"] = $_POST["year"];
					}

					if($_POST["month"] || $_GET["month"]) {
						$tmp = $_POST["month"] ? $_POST["month"] : $_GET["month"];
						$args["date_query"]["month"] = $_POST["month"];
					}

					$loop = new WP_Query($args);

					?>
					<?php if ($loop->have_posts()) : ?>
						<div class="row">
							<?php while ($loop->have_posts()) : $loop->the_post(); ?>
								<?php $file = get_post_meta(get_the_ID(), 'magazine_pdf', true); ?>
								<article <?php post_class("col-12 col-sm-6 magazine"); ?> id="magazine-<?php the_ID(); ?>">
									<a href="<?php echo $file["url"] ? $file["url"] : "javascript:void(0)" ?>" target="_blank">
										<div class="magazine-img-container">
											<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
										</div>
										<h5 class="magazine-title"><?php the_title(); ?></h5>
										<p class="magazine-date"><?php echo get_the_date("F Y"); ?></p>
									</a>
								</article>
							<?php endwhile; ?>
						</div>
					<?php else: ?>
						<div class="col-12">
							<p><?php _e("Nessun risultato trovato", "officineimmaginazione"); ?></p>
						</div>
					<?php endif; ?>
					<div>
						<?php oi_pagination(array("query" => $loop)); ?>
					</div>
			</div>
		</main>
		<?php if(is_active_sidebar("sidebar")) : ?>
			<aside class="col-12 col-md-4 sidebar">
				<?php do_shortcode(dynamic_sidebar("sidebar")); ?>
			</aside>
		<?php endif; ?>
	</div>
</section>

<?php $loading = __("Carico i risultati", "officineimmaginazione") . "..."; ?>	
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".btn-remove-filters").on("click",function(e){
			$('.pagination li a').each(function(){
				var href = $(this).attr('href');
				$(this).attr('href', href.substring(0, href.indexOf('?')));
			});
			var url = window.location.href;
			if (url.indexOf("/page") >= 0) {
				window.location.replace(url.substring(0, url.indexOf('/page')));
			}
			else {
				window.location.replace(url.substring(0, url.indexOf('?')));
			}
			$(".filter-select").prop("selectedIndex", 0).change();
		});
		$(".filter-select").on("change",function(e){
			var url = window.location.href;
			$loading = "<?php echo $loading; ?>";
			$cid = $("#cfilter").val();
			$year = $("#yfilter").val();
			$month = $("#mfilter").val();
			$("#content").hide(200, function(){$("#content").remove();});
			$("#category-magazine-main").html('<div class="loading"><p>' + $loading + '</p></div>').delay(250);
			$("#category-magazine-main").load(
				url.substring(0, url.indexOf('?')) +  " #content",
				{cid:$cid,year:$year,month:$month}, 
				function(responseTxt, statusTxt, xhr){
					if(statusTxt == "error")
						alert("Error: " + xhr.status + ": " + xhr.statusText);
				}
			).delay(400);
		});
		$(document).ajaxComplete(function() {
			$('.pagination li a').each(function(){
				var href = $(this).attr('href');
				if (href.indexOf("?cid") < 0) {
					$cid = $("#cfilter").val();
					$year = $("#yfilter").val();
					$month = $("#mfilter").val();
					$(this).attr("href", $(this).attr("href") + "?cid=" + $cid + "&year=" + $year + "&month=" + $month);
				}
			});
			$("#content .loading").hide(200, function(){$("#content .loading").remove();});
		});
        
        $(".filter-bar-content").mCustomScrollbar({
            theme:"dark",
            axis:"x",
			scrollbarPosition: "outside",
            mouseWheel:{ axis: "x" }
        });
        
        
	});
</script>