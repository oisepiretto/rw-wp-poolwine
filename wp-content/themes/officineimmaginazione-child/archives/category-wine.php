<?php 
$catid = get_query_var("cat");

if(isset($_POST["fid"]) || isset($_GET["fid"])) {
	if($_POST["fid"] || $_GET["fid"]) {
		$fid = $_POST["fid"] ? $_POST["fid"] : $_GET["fid"];
	}
}

if(isset($_POST["gid"]) || isset($_GET["gid"])) {
	if($_POST["gid"] || $_GET["gid"]) {
		$gid = $_POST["gid"] ? $_POST["gid"] : $_GET["gid"];
	}
}

if(isset($_POST["year"]) || isset($_GET["year"])) {
	if($_POST["year"] || $_GET["year"]) {
		$year = $_POST["year"] ? $_POST["year"] : $_GET["year"];
	}
}

if(isset($_POST["month"]) || isset($_GET["month"])) {
	if($_POST["month"] || $_GET["month"]) {
		$month = $_POST["month"] ? $_POST["month"] : $_GET["month"];
	}
}

?>
<?php if(have_posts()): ?> 
	<section class="container category-wine-intro">
		<?php
		wp_reset_query();
		$args = array(
			'post_type' 		=> 'post',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> 1,
			'orderby'			=> 'date',
			'order'				=> 'DESC',
			'suppress_filters'	=> false,
			'tax_query' => array(
				array(
					'taxonomy' 	=> 'category',
					'field' 	=> 'id',
					'terms' 	=> $catid
				)
			)
		);
		
		$first = new WP_Query($args);

		?>
		<?php while($first->have_posts()) : $first->the_post(); ?>
			<article <?php post_class("row align-items-center wine"); ?> id="wine-<?php the_ID(); ?>">
				<div class="col-12 col-md-4 col-lg-3">
					<h1 class="category-wine-title"><?php echo get_cat_name($catid); ?></h1>
					<h5 class="category-wine-date"><?php echo get_the_date("d F Y"); ?></h5>
					<hr />
					<div class="wine-title-container">
						<h3 class="wine-title"><?php the_title(); ?></h3>
						<?php 
						$type = get_post_meta(get_the_ID(), 'wd_type', true);
						$productor = get_post_meta(get_the_ID(), 'wd_productor', true); 
						
						?>
						<?php if($type) : ?>
							<p class="wine-type"><?php echo $type; ?></p>
						<?php endif; ?>
					</div>
					<div class="wine-productor-container">
						<?php if($productor) : ?>
							<h4 class="wine-productor-title"><?php _e("Produttore", "officineimmaginazione"); ?></h4>
							<p class="wine-productor-desc"><?php echo $productor; ?></p>
						<?php endif; ?>
					</div>
					<a class="btn btn-default" href="<?php the_permalink(); ?>"><?php _e("Scopri", "officineimmaginazione"); ?></a>
				</div>
				<div class="col-12 col-md-8 col-lg-6">
					<div class="wine-img-container">
						<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
					</div>
				</div>
				<div class="col-12 col-lg-3 wine-description">
					<?php if(get_the_content()) : ?>
						<div class="wine-excerpt-container">
							<h5 class="wine-intro-title"><?php _e("Descrizione", "officineimmaginazione"); ?></h5>
							<div class="wine-excerpt"><?php echo wp_trim_words(get_the_content(), 20); ?></div>
						</div>
					<?php endif; ?>
					<?php $location = get_post_meta(get_the_ID(), 'wd_location', true); ?>
					<?php if($location) : ?>
						<div class="mb-4">
							<h5 class="wine-intro-title"><?php _e("Location", "officineimmaginazione"); ?></h5>
							<p><?php echo $location; ?></p>
						</div>
					<?php endif; ?>
					<?php 
					$foods = wp_get_post_terms(get_the_ID(), "post_food"); 
					$song = array(
						"artist"	=> get_post_meta(get_the_ID(), 'wd_artist', true),
						"title"		=> get_post_meta(get_the_ID(), 'wd_song_title', true),
						"url"		=> get_post_meta(get_the_ID(), 'wd_song_url', true),
					);
					?>
					<?php if(array_filter($foods) || array_filter($song)) : ?>
						<div class="wine-match">
							<h5 class="wine-intro-title"><?php _e("Abbinamenti", "officineimmaginazione"); ?></h5>
                            <div class="row">
							<?php if(array_filter($foods)) : ?>
                                <div class="col-12 col-md-4 col-lg-12">
                                    <span class="d-block mt-3 mb-1"><?php _e("Food", "officineimmaginazione"); ?></span>
                                    <div class="d-flex align-items-center">
                                        <?php foreach($foods as $i => $food) : ?>
                                            <?php $food_img = get_option("post_food_" . $food->term_id)["image"]; ?>
                                            <?php if($i) : ?>
                                                <span class="plus">+</span>
                                            <?php endif; ?>
                                            <?php if($food_img) : ?>
                                                <img class="wine-icon" src="<?php echo $food_img; ?>" alt="<?php echo $food->name; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $food->name; ?>" />
                                            <?php else : ?>
                                                <span><?php echo $food->name; ?></span>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
							<?php endif; ?>
							<?php if(array_filter($song)) : ?>
                                <div class="col-12 col-md-6 col-lg-12">
                                    <span class="d-block mt-3 mb-1"><?php _e("Music", "officineimmaginazione"); ?></span>
                                    <a class="d-flex align-items-center wine-song" href="<?php echo $song["url"]; ?>" target="_blank">
                                        <div>
                                            <img class="wine-icon" src="<?php echo get_stylesheet_directory_uri() . "/assets/img/music-icon.svg"; ?>" alt="" />
                                        </div>
                                        <div>
                                            <?php if($song["artist"]) : ?>
                                                <strong class="d-block"><?php echo $song["artist"]; ?></strong>
                                            <?php endif; ?>
                                            <?php if($song["title"]) : ?>
                                                <span class="d-block"><?php echo $song["title"]; ?></span>
                                            <?php endif; ?>
                                        </div>
                                        <div>
                                            <i class="fas fa-play"></i>
                                        </div>
                                    </a>
                                </div>
							<?php endif; ?>
                            </div>
						</div>
					<?php endif; ?>
				</div>
			</article>
		<?php endwhile;?>
	</section>
<?php endif; ?>
<section class="filter-bar" id="filter-bar">
	<div class="container">
        <div class="row no-gutters">
            <div class="d-flex align-items-center filter-bar-title"><?php _e("Filtra per:", "officineimmaginazione") ?></div>
            <div class="filter-bar-content">
                <div>
                    <?php 
                    $yargs = array(
                        "posts_per_page" => -1,
                        "post_status" => "publish",
                        "oderby"	=> "date",
                        "order" => "ASC"
                    );
                    $oldest_year = (int)date("Y", strtotime(get_posts($yargs)[0]->post_date));
                    $current_year = (int)date("Y");
                    ?>
                    <select class="custom-select filter-select" id="yfilter" name="year_filter">
                        <option selected disabled><?php _e("Anno", "officineimmaginazione"); ?></option>
                        <?php for ($y = $oldest_year; $y <= $current_year; $y++) : ?>
                            <option value="<?php echo $y; ?>" <?php echo ($y == $year) ? "selected" : ""; ?>><?php echo $y; ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div>
                    <select class="custom-select filter-select" id="mfilter" name="month_filter">
                        <option selected disabled><?php _e("Mese", "officineimmaginazione"); ?></option>
                        <?php for ($m = 1; $m <= 12; $m++) : ?>
                            <option value="<?php echo $m; ?>" <?php echo ($m == $month) ? "selected" : ""; ?>><?php echo date_i18n('F', mktime(0, 0, 0, $m, 10)); ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div>
                    <?php
                    if($terms = get_terms('post_food', 'orderby=name')) : // to make it simple I use default categories
                        echo '<select class="custom-select filter-select" id="ffilter" name="food_filter"><option selected disabled>' . __("Abbinamento", "officineimmaginazione") . '</option>';
                        foreach ($terms as $term) {
							$selected = ($term->term_id == $fid) ? 'selected' : '';
                            echo '<option value="' . $term->term_id . '"' . $selected . '>' . $term->name . '</option>'; // ID of the category as the value of an option
						}
                        echo '</select>';
                    endif;
                    ?>
                </div>
                <div>
                    <?php
                    if($terms = get_terms('post_music_genre', 'orderby=name')) : // to make it simple I use default categories
                        echo '<select class="custom-select filter-select" id="gfilter" name="genre_filter"><option selected disabled>' . __("Genere musicale", "officineimmaginazione") . '</option>';
                        foreach ($terms as $term) {
							$selected = ($term->term_id == $gid) ? 'selected' : '';
                            echo '<option value="' . $term->term_id . '"' . $selected . '>' . $term->name . '</option>'; // ID of the category as the value of an option
						}
                        echo '</select>';
                    endif;
                    ?>
                </div>
                <div>
                    <button class="btn btn-default btn-remove-filters"><?php _e("Azzera", "officineimmaginazione"); ?></button>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="container">
	<div class="row">
		<main class="col-12 col-md-8 category-wine-main" id="category-wine-main">
			<div id="content">
				<?php
				wp_reset_query();
				$args = array(
					'post_type' 		=> 'post',
					'post_status' 		=> 'publish',
					'orderby'			=> 'date',
					'order'				=> 'DESC',
					'paged'				=> get_query_var('paged') ? get_query_var('paged') : 1,
					'suppress_filters'	=> false,
					'tax_query' => array(
						array(
							'taxonomy' 	=> 'category',
							'field' 	=> 'id',
							'terms' 	=> $catid
						)
					)
				);

				if($_POST["fid"] || $_GET["fid"]) {
					$tmp = $_POST["fid"] ? $_POST["fid"] : $_GET["fid"];
					$args["tax_query"][1] = array(
						"taxonomy" => "post_food",
						"field" => "id",
						"terms" => $tmp
					);
				}

				if($_POST["gid"] || $_GET["gid"]) {
					$tmp = $_POST["gid"] ? $_POST["gid"] : $_GET["gid"];
					$args["tax_query"][2] = array(
						"taxonomy" => "post_music_genre",
						"field" => "id",
						"terms" => $tmp
					);
				}
				
				if($_POST["year"] || $_GET["year"]) {
					$tmp = $_POST["year"] ? $_POST["year"] : $_GET["year"];
					$args["date_query"]["year"] = $_POST["year"];
				}
				
				if($_POST["month"] || $_GET["month"]) {
					$tmp = $_POST["month"] ? $_POST["month"] : $_GET["month"];
					$args["date_query"]["month"] = $_POST["month"];
				}

				$loop = new WP_Query($args);

				?>
				<?php if ($loop->have_posts()) : ?>
					<?php while ($loop->have_posts()) : $loop->the_post(); ?>
						<article <?php post_class("row wine"); ?> id="wine-<?php the_ID(); ?>">
							<div class="col-12 col-md-6">
								<a class="d-block" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
									<div class="wine-img-container">
										<?php echo get_tag_picture(get_post_thumbnail_id()); ?>
									</div>
								</a>
							</div>
							<div class="d-flex flex-wrap col-12 col-md-6 wine-details-container">
								<div class="align-self-start w-100 wine-title-box">
									<h5 class="wine-date"><?php echo get_the_date("d F Y"); ?></h5>
									<h3 class="wine-title">
										<a class="d-block" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
											<span class="d-block"><?php the_title(); ?></span>
											<?php $productor = get_post_meta(get_the_ID(), 'wd_productor', true); ?>
											<?php if($productor) : ?>
												<span class="d-block"><?php $productor; ?></span>
											<?php endif; ?>
										</a>
									</h3>
								</div>
								<div class="align-self-center w-100 wine-excerpt">
									<span class="d-block"><?php echo wp_trim_words(get_the_content(), 20); ?></span>
									<a class="btn-dots" href="<?php the_permalink(); ?>" ><span></span></a>
								</div>
								<div class="align-self-end w-100 share-box">
									<span><?php _e("Condividi", "officineimmaginazione"); ?></span>
									<span id="share-<?php echo get_the_ID() ?>" class="share"></span>
								</div>
								<script async type="text/javascript">
									jQuery(document).ready(function($) {
										$("#share-<?php echo get_the_ID() ?>").jsSocials({
											url: "<?php the_permalink(); ?>",
											showLabel: false,
											showCount: false,
											shares: [
												{share: "facebook", logo: "fab fa-facebook"},
												{share: "pinterest", logo: "fab fa-pinterest"}
											]
										});
										$(document).ajaxComplete(function() {
											$("#share-<?php echo get_the_ID() ?>").jsSocials("destroy");
											$("#share-<?php echo get_the_ID() ?>").jsSocials({
												url: "<?php the_permalink(); ?>",
												showLabel: false,
												showCount: false,
												shares: [
													{share: "facebook", logo: "fab fa-facebook"},
													{share: "pinterest", logo: "fab fa-pinterest"}
												]
											});
										});
									});
								</script>
							</div>
						</article>
					<?php endwhile; ?>
					<div>
						<?php oi_pagination(array("query" => $loop)); ?>
					</div>
				<?php else: ?>
					<div>
						<p><?php _e("Nessun risultato trovato", "officineimmaginazione"); ?></p>
					</div>
				<?php endif; ?>
				
			</div>
		</main>
		<?php if(is_active_sidebar("sidebar")) : ?>
			<aside class="col-12 col-md-4 sidebar">
				<?php do_shortcode(dynamic_sidebar("sidebar")); ?>
			</aside>
		<?php endif; ?>
	</div>
</section>
<?php wp_reset_query(); ?>
<?php $loading = __("Carico i risultati", "officineimmaginazione") . "..."; ?>	
<script type="text/javascript">
	jQuery(document).ready(function($) {
		
		$('[data-toggle="tooltip"]').tooltip();
		
		$(".btn-remove-filters").on("click",function(e){
			$('.pagination li a').each(function(){
				var href = $(this).attr('href');
				$(this).attr('href', href.substring(0, href.indexOf('?')));
			});
			var url = window.location.href;
			if (url.indexOf("/page") >= 0) {
				window.location.replace(url.substring(0, url.indexOf('/page')));
			}
			else {
				window.location.replace(url.substring(0, url.indexOf('?')));
			}
			$(".filter-select").prop("selectedIndex", 0).change();
		});
		
		$(".filter-select").on("change",function(e){
			var url = window.location.href;
			$loading = "<?php echo $loading; ?>";
			$fid = $("#ffilter").val();
			$gid = $("#gfilter").val();
			$year = $("#yfilter").val();
			$month = $("#mfilter").val();
			if($fid || $gid || $year || $month)
				$offset = 0;
			else
				$offset = 1;
			$("#content").hide(200, function(){$("#content").remove();});
			$("#category-wine-main").html('<div class="loading"><p>' + $loading + '</p></div>').delay(250);
			$("#category-wine-main").load(
				url.substring(0, url.indexOf('?')) +  " #content",
				{offset:$offset,fid:$fid,gid:$gid,year:$year,month:$month}, 
				function(responseTxt, statusTxt, xhr){
					if(statusTxt == "error")
						alert("Error: " + xhr.status + ": " + xhr.statusText);
				}
			).delay(400);
		});
		
		
		$(document).ajaxComplete(function() {
			$('.pagination li a').each(function(){
				var href = $(this).attr('href');
				if (href.indexOf("?offset") < 0) {
					$fid = $("#ffilter").val();
					$gid = $("#gfilter").val();
					$year = $("#yfilter").val();
					$month = $("#mfilter").val();
					if($fid || $gid || $year || $month)
						$offset = 0;
					else
						$offset = 1;
					$(this).attr("href", href + "?offset=" + $offset + "&fid=" + $fid + "&gid=" + $gid + "&year=" + $year + "&month=" + $month);
				}
			});
			$("#content .loading").hide(200, function(){$("#content .loading").remove();});
		});
        
        
        $(".filter-bar-content").mCustomScrollbar({
            theme:"dark",
            axis:"x",
			scrollbarPosition: "outside",
            mouseWheel:{ axis: "x" }
        });
        
	});
</script>