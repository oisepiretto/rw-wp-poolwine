<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'poolwine_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mSfj#lNI,^=_h6;_Dba:6qy45 :f+F%[PN}Al+L%CjeL`]uA`~6+rWGgdr6B+|hV');
define('SECURE_AUTH_KEY',  'w@Kj]^9$$>Qim8ZvX&]V;ysX7/aPi/l Y[WtmEhG<Y>8syPiY7+vFsmb?Jt%9o,2');
define('LOGGED_IN_KEY',    '1I@G4{z{Knmq^*&+NVwS`m6-a )Ji61sCaFS`Z[0%bs6WF>/pw?2%)|mFy>pc<;<');
define('NONCE_KEY',        '@Ga5wgmN,t,diyaqA %%jZ*jbH(~zPvu^:Tx~?[7&Xb&`#TmjVO=kn<]2_&C Bm@');
define('AUTH_SALT',        'rhI-~_}5XxAflxk9h![i1q8)z)1@HsF|3p/V-Y6*,voWS(WxvO,0`NHL_h#vb)aI');
define('SECURE_AUTH_SALT', 'CSdVHQab?l[w#-NzEc<&!GF<OC,rW!kitMR/M:zFJDt?P1kBLDZ*7ays/b<[insH');
define('LOGGED_IN_SALT',   'M,K%}9(%-[esB^1TJ}2[zs-We,M[MOc;8SSj]jiTQdiX.@l$XUi4Pch<5p)(@ 1$');
define('NONCE_SALT',       '_7CFl;/1j).9y_a5yUJJB>9<t.wd5iAJk`lH^GHSG8tu7{F{WkY:TZp]A?73)C5Q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
